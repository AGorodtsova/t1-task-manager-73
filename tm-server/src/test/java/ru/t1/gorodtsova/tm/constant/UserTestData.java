package ru.t1.gorodtsova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN1 = new UserDTO();

    @NotNull
    public final static List<UserDTO> USER_LIST1 = Arrays.asList(USER1, USER2);

    @NotNull
    public final static List<UserDTO> USER_LIST2 = Collections.singletonList(ADMIN1);

    @NotNull
    public final static List<UserDTO> USER_LIST = new ArrayList<>();

    static {
        USER_LIST.addAll(USER_LIST1);
        USER_LIST.addAll(USER_LIST2);

        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final UserDTO user = USER_LIST.get(i);
            user.setId("t-0" + i);
            user.setLogin("login" + i);
            user.setEmail("user" + i + "@company.ru");
        }
    }

}
