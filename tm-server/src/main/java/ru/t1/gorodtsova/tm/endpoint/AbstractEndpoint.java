package ru.t1.gorodtsova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.gorodtsova.tm.api.service.IAuthService;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;
import ru.t1.gorodtsova.tm.dto.request.AbstractUserRequest;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.exception.user.AccessDeniedException;


@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}
