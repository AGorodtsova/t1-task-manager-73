package ru.t1.gorodtsova.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Transactional
    M add(@Nullable String userId, M model);

    @NotNull
    List<M> findAll(@Nullable String userId);

    M findOneById(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeAll(@Nullable String userId);

    @Transactional
    void removeOne(@Nullable String userId, M model);

    @Transactional
    void removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    long getSize(@Nullable String userId);

}
