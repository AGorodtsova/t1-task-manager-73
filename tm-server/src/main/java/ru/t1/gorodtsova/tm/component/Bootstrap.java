package ru.t1.gorodtsova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.service.*;
import ru.t1.gorodtsova.tm.api.service.dto.*;
import ru.t1.gorodtsova.tm.endpoint.AbstractEndpoint;
import ru.t1.gorodtsova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IUserDtoService userService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    public IDomainService domainService;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK_MANAGER IS SHUTTING DOWN **");
        //backup.stop();
    }

    public void start() {
        initEndpoints();
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        loggerService.startJmsLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //backup.start();
    }

}
