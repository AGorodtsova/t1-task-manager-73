package ru.t1.gorodtsova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
