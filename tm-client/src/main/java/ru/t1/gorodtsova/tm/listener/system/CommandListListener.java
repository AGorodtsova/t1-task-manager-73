package ru.t1.gorodtsova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.IListener;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show command list";

    @NotNull
    private final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@Nullable final IListener listener : listeners) {
            if (listener == null) continue;
            @Nullable final String name = listener.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
