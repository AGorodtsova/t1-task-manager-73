package ru.t1.gorodtsova.tm.exception.user;

import ru.t1.gorodtsova.tm.exception.AbstractException;

public final class PasswordEmptyException extends AbstractException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
