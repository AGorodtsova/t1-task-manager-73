package ru.t1.gorodtsova.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
