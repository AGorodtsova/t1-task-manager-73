package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("api/tasks")
@WebService(endpointInterface = "ru.t1.gorodtsova.tm.api.endpoint.ITaskRestEndpoint")
public class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.saveByUserId(task, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        taskService.clearByUserId(UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

}
