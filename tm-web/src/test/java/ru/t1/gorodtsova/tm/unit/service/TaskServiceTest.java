package ru.t1.gorodtsova.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.config.DataBaseConfiguration;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.util.UserUtil;

import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class TaskServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Task task1 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task task2 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task task3 = new Task(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.saveByUserId(task1, UserUtil.getUserId());
        taskService.saveByUserId(task2, UserUtil.getUserId());
    }

    @After
    public void clean() {
        taskService.clearByUserId(UserUtil.getUserId());
    }

    @Test
    public void saveTest() {
        taskService.save(task3);
        Assert.assertEquals(task3.getName(), taskService.findById(task3.getId()).getName());
        task3.setName(UUID.randomUUID().toString());
        taskService.save(task3);
        Assert.assertEquals(task3.getName(), taskService.findById(task3.getId()).getName());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserIdTest() {
        Assert.assertNotNull(taskService.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeAllByUserIdTest() {
        taskService.clearByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void removeOneByUserIdTest() {
        taskService.removeByUserId(task2, UserUtil.getUserId());
        Assert.assertNull(taskService.findByIdAndUserId(task2.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserIdTest() {
        taskService.removeByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskService.findByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

}
